package kz.daurenidrissov.testcoroutines

import retrofit2.Response
import retrofit2.http.GET

private const val BASE_EUR = "latest?base=EUR"
private const val BASE_USD = "latest?base=USD"

interface ConverterApiService {
    @GET(BASE_EUR)
    suspend fun getEur(): CurrencyData

    @GET(BASE_USD)
    suspend fun getUsd(): CurrencyData
}