package kz.daurenidrissov.testcoroutines

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    private val BASE_URL = "https://hiring.revolut.codes/api/android/"

    val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ConverterApiService::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        CoroutineScope(IO).launch {
            try {
                val currencies_usd = async { getUsd() }
                val currencies_eur = async { getEur() }

                withContext(Main) {
                    usdTextView.text = currencies_usd.await().toString()
                    euroTextView.text = currencies_eur.await().toString()
                }
            } catch (e: NullPointerException) {
                Toast.makeText(applicationContext, "Error! ${e.message}", Toast.LENGTH_LONG).show()
            }
        }
    }

    private suspend fun getEur(): CurrencyData {
        if (retrofit.getEur() != null) {
            return retrofit.getEur()
        } else {
            throw NullPointerException("Error!")
        }
    }

    private suspend fun getUsd(): CurrencyData {
        if (retrofit.getUsd() != null) {
            return retrofit.getUsd()
        } else {
            throw NullPointerException("Error!")
        }
    }

}

